package me.reider45.Archery.Handlers;

import java.util.HashSet;

import org.bukkit.entity.Player;

import me.reider45.Archery.Objects.IPlayer;

public class PlayerManager {
	
	private HashSet<IPlayer> players = new HashSet<IPlayer>();
	
	public PlayerManager() {
		players = new HashSet<IPlayer>();
	}

	// Return profile
	public IPlayer getProfile(Player p) {
		for(IPlayer a : players)
			if(a.getPlayer().getName().equals(p.getName()))
				return a;

		return null;
	}
	
	public void addPlayer(IPlayer ip) {
		players.add(ip);
	}
	
	public void remPlayer(IPlayer ip) {
		players.remove(ip);
	}
	
	public void shutdown() {
		players.clear();
	}


}
