package me.reider45.Gadgets.Events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import me.reider45.Gadgets.Gadgets;
import me.reider45.Gadgets.Handlers.Menu;

public class MenuEvent implements Listener {

	private Gadgets main;

	public MenuEvent(Gadgets instance) {
		main = instance;
	}

	@EventHandler
	public void onMenuOpen(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		ItemStack inHand = p.getInventory().getItemInMainHand();
		if (inHand != null && inHand.isSimilar(Menu.ITEM_MENU)) {
			e.setCancelled(true);
			p.openInventory(Menu.INVENTORY_MENU);

		}
	}

	@EventHandler
	public void onChoose(InventoryClickEvent e) {
		if (e.getInventory().getName().equals(Menu.INVENTORY_MENU.getName())) {
			e.setCancelled(true);

			Player p = (Player) e.getWhoClicked();
			ItemStack clicked = e.getCurrentItem();

			if (clicked != null && main.getGadgetHandler().isGadget(clicked)) {

				// Equip the gadget they've selected
				main.getGadgetHandler().equipGadget(p,
						main.getGadgetHandler().getGadget(clicked.getItemMeta().getDisplayName()));

				p.closeInventory();
			} else

			if (clicked.isSimilar(Menu.ITEM_REMOVE)) {

				// Take off their gadget
				main.getGadgetHandler().removeGadget(p);
				p.closeInventory();

			}

		}
	}

	@EventHandler
	public void onInventoryClick(InventoryClickEvent e) {
		ItemStack currentItem = e.getCurrentItem();
		if (currentItem != null && (currentItem.isSimilar(Menu.ITEM_MENU) || currentItem.getType() == Material.ARROW
				|| main.getGadgetHandler().isGadget(currentItem))) {
			e.setCancelled(true);
		}
	}

}
