package me.reider45.Gadgets.Weapons;

import me.reider45.Gadgets.Gadgets;
import me.reider45.Gadgets.Handlers.CooldownHandler;
import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Gadget {

	public static Gadgets main;
	private Material weapon_mat;
	private String weapon_name;
	private Long weapon_cooldown;
	private ItemStack stack;

	private Long current_cooldown;

	public Gadget(Material mat, String name, long cooldown) {
		this.weapon_mat = mat;
		this.weapon_name = name;
		this.weapon_cooldown = cooldown;
		this.current_cooldown = cooldown;

		this.stack = new ItemStack(mat, 1);
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		stack.setItemMeta(meta);

		getGadgetHandler().getGadgets().add(this);
	}

	public ItemStack getItem() {
		return this.stack;
	}

	public Material getMaterial() {
		return this.weapon_mat;
	}

	public String getName() {
		return this.weapon_name;
	}

	public Long getCooldown() {
		return this.weapon_cooldown;
	}

	public Long getCurrentCooldown() {
		return this.current_cooldown;
	}

	public GadgetHandler getGadgetHandler() {
		return main.getGadgetHandler();
	}
	
	public CooldownHandler getCooldownHandler() {
		return main.getCooldownHandler();
	}

}
