package me.reider45.Archery.Arrows.Events;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import me.reider45.Archery.Archery;
import me.reider45.Archery.Objects.IPlayer;

public class ArrowSwap implements Listener {

	@EventHandler
	public void onSwap(PlayerInteractEvent e) {

		if (!e.getAction().name().contains("LEFT"))
			return;

		Player p = e.getPlayer();

		if (p.getItemInHand().getType() != Material.BOW)
			return;

		IPlayer pa = Archery.getPM().getProfile(p);

		Archery.getAM().swapArrow(pa.getArrow(), p);

	}

}
