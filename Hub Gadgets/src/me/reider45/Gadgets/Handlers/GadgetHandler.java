package me.reider45.Gadgets.Handlers;

import java.util.HashSet;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import me.reider45.Gadgets.Weapons.Gadget;

public class GadgetHandler {

	private HashSet<Gadget> gadgets;

	public GadgetHandler() {
		gadgets = new HashSet<>();
	}

	public HashSet<Gadget> getGadgets() {
		return gadgets;
	}

	public void addGadget(Gadget g) {
		gadgets.add(g);
	}

	public void removeGadget(Gadget g) {
		gadgets.remove(g);
	}

	public Boolean isGadget(ItemStack i) {
		for (Gadget g : gadgets) {
			if (g.getItem().isSimilar(i))
				return true;
		}
		return false;
	}

	public Gadget hasGadget(Player p) {
		for (ItemStack i : p.getInventory().getContents()) {
			if (isGadget(i))
				return getGadget(i.getItemMeta().getDisplayName());
		}
		return null;
	}

	public Gadget getGadget(String name) {
		for (Gadget g : gadgets) {
			if (g.getName().equals(name))
				return g;
		}
		return null;
	}

	public void removeGadget(Player p) {
		p.getInventory().setArmorContents(null);

		p.getInventory().remove(Material.ARROW);
		p.getInventory().setItem(0, null);
	}

	public void equipGadget(Player p, Gadget g) {
		removeGadget(p);

		if (g.getName().equals("Bow Popper")) {
			p.getInventory().setItem(9, new ItemStack(Material.ARROW, 1));
		}

		p.getInventory().setItem(0, g.getItem());
	}

}
