package me.reider45.Gadgets.Events;

import me.reider45.Gadgets.Handlers.Menu;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerEvent implements Listener {
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		e.getPlayer().getInventory().setItem(8, Menu.ITEM_MENU);
	}

}
