package me.reider45.Gadgets.Weapons;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Bat;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import me.reider45.Gadgets.Handlers.Cooldown;

public class GadgetBat extends Gadget implements Listener {

	private Gadget bat;

	public GadgetBat() {
		super(Material.IRON_BARDING, "Bat Blaster", 4);

		bat = getGadgetHandler().getGadget("Bat Blaster");
	}

	// Shoot
	@EventHandler
	public void onShoot(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (e.getAction().name().contains("RIGHT")) {

			ItemStack inHand = p.getInventory().getItemInMainHand();

			if (inHand != null && bat.getItem().isSimilar(inHand)) {

				// On CD still
				if (getCooldownHandler().hasCooldown(p, bat)) {
					return;
				}

				new Cooldown(bat, p);

				// Fire

				final List<Bat> bats = new ArrayList<Bat>();
				for (int i = 0; i < 30; i++) {
					bats.add(p.getWorld().spawn(p.getEyeLocation(), Bat.class));
				}

				new BukkitRunnable() {
					public void run() {
						for (Bat b : bats) {
							b.remove();
						}
					}
				}.runTaskLater(main, 20L);

			}
		}

	}

}
