package me.reider45.KingdomWhitelist;

import java.util.Calendar;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandHandler implements CommandExecutor {
	
	Main pl;
	public CommandHandler(Main ms) {
		pl = ms;
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (sender instanceof Player) {

			Player p = (Player) sender;
			if (cmd.getName().equalsIgnoreCase("wl")) {

				// Whitelist command
				if (args.length == 1) {

					// wl on
					if (args[0].equals("on")) {
						if (p.hasPermission("whitelist.on")) {
							pl.enabled = true;
							p.sendMessage(pl.getConfig().getString("Whitelist.OnMSG").replace('$', '�'));
						}

					} else
					// wl off
					if (args[0].equals("off")) {
						if (p.hasPermission("whitelist.off")) {
							pl.enabled = false;
							p.sendMessage(pl.getConfig().getString("Whitelist.OffMSG").replace('$', '�'));
						}
					}
				} else if (args.length == 3) {
					if (args[0].equals("set")) {

						// wl set time
						if (args[1].equals("time")) {
							String time = args[2];

							Integer hours, mins, secs;

							if (!time.contains("h") || !time.contains("m") || !time.contains("s")) {
								p.sendMessage("Invalid format! Use 00h00m00s!");
								return false;
							}

							Integer hpos, mpos, spos;
							hpos = time.indexOf("h");
							mpos = time.indexOf("m");
							spos = time.indexOf("s");

							if (hpos > mpos || mpos > spos) {
								p.sendMessage("Invalid format! Use 00h00m00s!");
								return false;
							}

							hours = Integer.parseInt(time.split("h")[0]);
							time = time.replaceFirst(hours + "h", "");

							mins = Integer.parseInt(time.split("m")[0]);
							time = time.replaceFirst(mins + "m", "");

							secs = Integer.parseInt(time.split("s")[0]);

							p.sendMessage(pl.getConfig().getString("Whitelist.Set").replace("$", "�")
									.replace("%HOURS%", hours + "").replace("%MINS%", mins + "")
									.replace("%SECS%", secs + ""));

							Calendar cal = Calendar.getInstance();
							cal.set(Calendar.HOUR, cal.get(Calendar.HOUR) + hours);
							cal.set(Calendar.MINUTE, cal.get(Calendar.MINUTE) + mins);
							cal.set(Calendar.SECOND, cal.get(Calendar.SECOND) + secs);

							pl.toEnd = cal.getTimeInMillis();
						}
					}
				}
			}
		}
		return false;
	}

}
