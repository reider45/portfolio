package me.reider45.Archery.Objects;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.reider45.Archery.Arrows.ArrowType;

public class IArrow {

	private String name;
	private String displayName;
	private ItemStack arrow;
	private ItemMeta meta;
	private ArrowType type;

	public IArrow(String name, ArrowType type, String displayName) {
		this.name = name;
		this.type = type;
		this.displayName = displayName;
		this.arrow = new ItemStack(Material.ARROW, 1);

		ItemMeta meta = arrow.getItemMeta();
		meta.setDisplayName(displayName);
		arrow.setItemMeta(meta);
		
		this.meta = meta;

	}

	public String getName() {
		return name;
	}

	public String getDisplayName() {
		return displayName;
	}
	
	public ArrowType getType() {
		return type;
	}

	public ItemStack getArrow(int amount) {
		ItemStack toSend;
		toSend = new ItemStack(Material.ARROW, amount);
		toSend.setItemMeta(meta);
		return toSend;
	}

}
