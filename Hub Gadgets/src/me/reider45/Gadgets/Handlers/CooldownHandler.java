package me.reider45.Gadgets.Handlers;

import java.util.HashMap;

import me.reider45.Gadgets.Weapons.Gadget;

import org.bukkit.entity.Player;

public class CooldownHandler {

	private HashMap<Player, Cooldown> cooldowns;

	public CooldownHandler() {
		cooldowns = new HashMap<>();
	}
	
	public void addCooldown(Player p, Cooldown cooldown) {
		cooldowns.put(p, cooldown);
	}
	
	public void removeCooldown(Player p) {
		cooldowns.remove(p);
	}

	public Cooldown getCooldown(Player p) {
		return cooldowns.get(p);
	}

	public boolean hasCooldown(Player p, Gadget g) {
		if (cooldowns.containsKey(p)) {
			Cooldown cd = getCooldown(p);
			return cd.getGadget().getItem().isSimilar(g.getItem());
		}
		return false;
	}
}
