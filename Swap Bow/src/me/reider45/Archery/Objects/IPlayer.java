package me.reider45.Archery.Objects;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.reider45.Archery.Archery;

public class IPlayer {

	private Player p;
	private IArrow stored;
	private ItemStack bow;

	public IPlayer(Player p, IArrow arrow, ItemStack bow) {
		this.p = p;
		this.stored = arrow;
		this.bow = bow;

		ItemMeta meta = bow.getItemMeta();
		meta.setDisplayName("�e�lEquipped Arrow >> �cNone");
		bow.setItemMeta(meta);

		Archery.getPM().addPlayer(this);
	}

	public Player getPlayer() {
		return p;
	}

	public IArrow getArrow() {
		return stored;
	}

	public void setArrow(IArrow i) {
		this.stored = i;
		updateBow();
	}

	public ItemStack updateBow() {

		ItemMeta meta = bow.getItemMeta();
		meta.setDisplayName("�e�lEquipped Arrow >> " + this.getArrow().getDisplayName().split(" ")[0]);
		bow.setItemMeta(meta);

		if(p.getItemInHand() != null)
			p.getItemInHand().setItemMeta(meta);

		return bow;
	}

}
