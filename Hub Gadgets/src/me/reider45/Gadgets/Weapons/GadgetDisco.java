package me.reider45.Gadgets.Weapons;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class GadgetDisco extends Gadget implements Listener {

	public GadgetDisco() {
		super(Material.LEATHER_CHESTPLATE, "Disco Armor", 0);

		globalArmor();
	}

	// Runnable for armor changing
	private void globalArmor() {
		new BukkitRunnable() {
			public void run() {

				for (Player p : Bukkit.getOnlinePlayers()) {
					if (getGadgetHandler().hasGadget(p) != null) {
						if (getGadgetHandler().hasGadget(p).getName().equals("Disco Armor")) {

							p.getInventory().setHelmet(randomColor(new ItemStack(Material.LEATHER_HELMET, 1)));
							p.getInventory().setChestplate(randomColor(new ItemStack(Material.LEATHER_CHESTPLATE, 1)));
							p.getInventory().setLeggings(randomColor(new ItemStack(Material.LEATHER_LEGGINGS, 1)));
							p.getInventory().setBoots(randomColor(new ItemStack(Material.LEATHER_BOOTS, 1)));

						}
					}
				}
			}
		}.runTaskTimer(main, 5, 5);
	}

	// Generate a random color for armor
	private ItemStack randomColor(ItemStack i) {
		LeatherArmorMeta meta = (LeatherArmorMeta) i.getItemMeta();

		Random r = new Random();
		meta.setColor(Color.fromRGB(r.nextInt(256), r.nextInt(256), r.nextInt(256)));
		i.setItemMeta(meta);

		return i;
	}

}
