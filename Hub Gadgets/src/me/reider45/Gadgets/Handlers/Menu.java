package me.reider45.Gadgets.Handlers;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import me.reider45.Gadgets.Gadgets;
import me.reider45.Gadgets.Weapons.Gadget;

public class Menu {

	public static Inventory INVENTORY_MENU;

	public static ItemStack ITEM_MENU;
	public static ItemStack ITEM_REMOVE;

	private Gadgets main;

	public Menu(Gadgets instance) {
		main = instance;
	}

	public void registerInventory() {
		ITEM_REMOVE = new ItemStack(Material.GOLD_BLOCK, 1);
		ItemMeta meta = ITEM_REMOVE.getItemMeta();
		meta.setDisplayName("�4Remove");
		ITEM_REMOVE.setItemMeta(meta);

		ITEM_MENU = new ItemStack(Material.CHEST, 1);
		meta = ITEM_MENU.getItemMeta();
		meta.setDisplayName("�6Gadget Menu");
		ITEM_MENU.setItemMeta(meta);

		INVENTORY_MENU = Bukkit.createInventory(null, 9, "Gadget Menu");
		INVENTORY_MENU.setItem(8, ITEM_REMOVE);

		int i = 0;
		for (Gadget g : main.getGadgetHandler().getGadgets()) {
			INVENTORY_MENU.setItem(i, g.getItem());
			i++;
		}
	}

}
