Hello! Welcome to my portfolio, my name is Reid and I've been a java/spigot developer for a few years now. I've worked for networks such as
http://cubecraftgames.net
http://noble-craft.net

and many others. I have been involved in numerous programming projects, most of them privately ran.
In this git, you will see a few older projects of mine to get an idea of my programming, I'm also the creator of MineSweeper and ConnectFour, able to be found here.
https://www.spigotmc.org/members/reider45.5589/

I'm knowledgeable in Java, MySQL, some Bungee, and the basics of GIT.

Since most of my projects are privately created and I'm not able to show them publicly, if you'd like to get an idea of my recent coding styles or formatting, feel free to ask!

If you'd like to contact me, my skype is richreid45, or email me at reid.campolong1@gmail.com
Thanks for your consideration!

-Reid