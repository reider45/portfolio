package me.reider45.Archery.Arrows.Events;

import org.bukkit.entity.Arrow;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ProjectileHitEvent;

import me.reider45.Archery.Archery;
import me.reider45.Archery.Objects.IArrow;

public class Lightning implements Listener {

	@EventHandler
	public void onHit(ProjectileHitEvent e) {
		if (!(e.getEntity() instanceof Arrow))
			return;

		IArrow arrow = Archery.getAM().isArrow((Arrow) e.getEntity());

		if (arrow == null)
			return;
		if (e.getEntity().hasMetadata("lightning"))
			e.getEntity().getLocation().getWorld().strikeLightning(e.getEntity().getLocation());

	}

}
