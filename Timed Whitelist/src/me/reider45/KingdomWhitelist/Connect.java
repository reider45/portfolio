package me.reider45.KingdomWhitelist;

import java.util.Calendar;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

public class Connect implements Listener {

	Main pl;

	public Connect(Main ms) {
		pl = ms;
		Bukkit.getPluginManager().registerEvents(this, ms);
	}

	@EventHandler
	public void onConnect(PlayerLoginEvent e) {

		if (!pl.enabled)
			return;
		if (!e.getPlayer().hasPermission("whitelist.bypass")) {
				long saved = pl.toEnd;

				long difference = saved - Calendar.getInstance().getTimeInMillis(); // Then
																					// minus
																					// now

				double hours = Math.floor((difference / 3600000) % 24);
				double mins = Math.floor((difference / 60000) % 60);
				double secs = Math.floor((difference / 1000) % 60);

				String time = pl.getConfig().getString("Whitelist.Kick").replace("$", "�").replace("%HOURS%", hours + "")
						.replace("%MINS%", mins + "").replace("%SECS%", secs + "");

				e.disallow(Result.KICK_OTHER, time);
			} else {
				e.disallow(Result.KICK_OTHER, pl.getConfig().getString("Whitelist.NoTime").replace("$", "�"));
			}
	}

}
