package me.reider45.Gadgets;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import me.reider45.Gadgets.Events.GadgetEvent;
import me.reider45.Gadgets.Events.MenuEvent;
import me.reider45.Gadgets.Events.PlayerEvent;
import me.reider45.Gadgets.Handlers.CooldownHandler;
import me.reider45.Gadgets.Handlers.GadgetHandler;
import me.reider45.Gadgets.Handlers.Menu;
import me.reider45.Gadgets.Weapons.Gadget;
import me.reider45.Gadgets.Weapons.GadgetBat;
import me.reider45.Gadgets.Weapons.GadgetBow;
import me.reider45.Gadgets.Weapons.GadgetDisco;
import me.reider45.Gadgets.Weapons.GadgetFlesh;
import me.reider45.Gadgets.Weapons.GadgetPearl;

public class Gadgets extends JavaPlugin {

	private GadgetHandler gadgetHandler;
	private CooldownHandler cooldownHandler;

	public void onEnable() {
		Gadget.main = this;
		gadgetHandler = new GadgetHandler();
		cooldownHandler = new CooldownHandler();

		registerListener(new GadgetEvent(this));
		registerListener(new MenuEvent(this));
		registerListener(new PlayerEvent());

		registerListener(new GadgetBow());
		registerListener(new GadgetBat());
		registerListener(new GadgetFlesh());
		registerListener(new GadgetPearl());
		registerListener(new GadgetDisco());

		new Menu(this).registerInventory();
	}

	public void registerListener(Listener listener) {
		Bukkit.getPluginManager().registerEvents(listener, this);
	}
	
	public GadgetHandler getGadgetHandler() {
		return gadgetHandler;
	}
	
	public CooldownHandler getCooldownHandler() {
		return cooldownHandler;
	}

}
