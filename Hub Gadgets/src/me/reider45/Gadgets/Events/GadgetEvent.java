package me.reider45.Gadgets.Events;

import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

public class GadgetEvent implements Listener {
	
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		if(GadgetHandler.isGadget(e.getItemDrop().getItemStack())){
			e.setCancelled(true);
		}
	}

}
