package me.reider45.Archery.Arrows.Events;

import org.bukkit.Location;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

import me.reider45.Archery.Archery;
import me.reider45.Archery.Objects.IArrow;

public class Swapping implements Listener {

	@EventHandler
	public void onHit(EntityDamageByEntityEvent e) {

		if (!(e.getDamager() instanceof Arrow))
			return;

		Arrow shotArrow = (Arrow) e.getDamager();
		IArrow arrow = Archery.getAM().isArrow(shotArrow);

		if (arrow == null)
			return;

		if (shotArrow.hasMetadata("swap")) {

			Player shooter = (Player) ((Arrow) e.getDamager()).getShooter();

			Location shooterPos = shooter.getLocation().clone();

			shooter.teleport(e.getEntity());
			e.getEntity().teleport(shooterPos);

		}

	}
}
