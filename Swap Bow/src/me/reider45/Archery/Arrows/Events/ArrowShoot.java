package me.reider45.Archery.Arrows.Events;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.metadata.FixedMetadataValue;

import me.reider45.Archery.Archery;
import me.reider45.Archery.Objects.IPlayer;

public class ArrowShoot implements Listener {

	@EventHandler
	public void onShoot(EntityShootBowEvent e) {
		if (!(e.getEntity() instanceof Player))
			return;

		IPlayer pa = Archery.getPM().getProfile((Player) (e.getEntity()));

		if (pa == null)
			return;

		switch (pa.getArrow().getName()) {
		case "lightning":
			e.getProjectile().setMetadata("lightning", new FixedMetadataValue(Archery.i(), true));
			break;
		case "swap":
			e.getProjectile().setMetadata("swap", new FixedMetadataValue(Archery.i(), true));
			break;
		}
	}

}
