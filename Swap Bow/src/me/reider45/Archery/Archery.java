package me.reider45.Archery;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import me.reider45.Archery.Arrows.Events.ArrowShoot;
import me.reider45.Archery.Arrows.Events.ArrowSwap;
import me.reider45.Archery.Arrows.Events.Lightning;
import me.reider45.Archery.Arrows.Events.Swapping;
import me.reider45.Archery.Handlers.ArrowManager;
import me.reider45.Archery.Handlers.PlayerManager;
import me.reider45.Archery.Objects.IPlayer;

public class Archery extends JavaPlugin {
	
	private static Archery pl;
	private static PlayerManager pm;
	private static ArrowManager am;
	
	public void onEnable() {
		pl = this;
		
		// Create Objects
		am = new ArrowManager();
		pm = new PlayerManager();
		
		// Load arrow events
		Bukkit.getPluginManager().registerEvents(new Lightning(), this);
		Bukkit.getPluginManager().registerEvents(new Swapping(), this);
		Bukkit.getPluginManager().registerEvents(new ArrowShoot(), this);
		Bukkit.getPluginManager().registerEvents(new ArrowSwap(), this);
		
		// This is for dev purposes to spawn in arrows on start
		for(Player p : Bukkit.getOnlinePlayers()){
			p.getInventory().addItem(getAM().getArrow("lightning").getArrow(10));
			p.getInventory().addItem(getAM().getArrow("swap").getArrow(10));
			new IPlayer(p, getAM().getArrow("lightning"), new ItemStack(Material.BOW, 1));
			p.getInventory().addItem(getPM().getProfile(p).updateBow());
		}
		
	}
	
	public void onDisable() {
		am.shutdown();
		am = null;
		
		pm.shutdown();
		pm = null;
		
		pl = null;
	}
	
	public static Archery i() {
		return pl;
	}
	
	public static PlayerManager getPM() {
		return pm;
	}
	
	public static ArrowManager getAM() {
		return am;
	}

}
