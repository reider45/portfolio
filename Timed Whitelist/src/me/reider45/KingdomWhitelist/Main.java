package me.reider45.KingdomWhitelist;

import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin {

	public Long toEnd;
	public Boolean enabled;

	public void onEnable() {
		saveDefaultConfig();
		
		toEnd = 0L;
		enabled = false;
		
		new Connect(this);
		getCommand("wl").setExecutor(new CommandHandler(this));
	}

	public void onDisable() {
		saveConfig();
	}

	

}
