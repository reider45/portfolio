package me.reider45.Gadgets.Handlers;

import java.util.HashMap;

import me.reider45.Gadgets.Weapons.Gadget;

import org.bukkit.entity.Player;

public class CooldownHandler {

	public static HashMap<Player, Cooldown> cooldowns = new HashMap<Player, Cooldown>();
	
	public static Cooldown getCooldown(Player p){
		return cooldowns.get(p);
	}
	
	public static Boolean hasCooldown(Player p, Gadget g){
		if(cooldowns.containsKey(p)){
			Cooldown cd = getCooldown(p);
			
			if(cd.getGadget().getItem().isSimilar(g.getItem())){
				return true;
			}
			
		}
		return false;
	}
}
