package me.reider45.Gadgets.Weapons;

import java.util.Set;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import me.reider45.Gadgets.Handlers.Cooldown;

public class GadgetFlesh extends Gadget implements Listener {

	private Gadget flesh;

	public GadgetFlesh() {
		super(Material.TRIPWIRE_HOOK, "Flesh Hook", 5);

		flesh = getGadgetHandler().getGadget("Flesh Hook");
	}

	// Shoot
	@EventHandler
	public void onShoot(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (e.getAction().name().contains("RIGHT")) {

			ItemStack inHand = p.getInventory().getItemInMainHand();

			if (inHand != null && flesh.getItem().isSimilar(inHand)) {
				e.setCancelled(true);

				// On CD still
				if (getCooldownHandler().hasCooldown(p, flesh)) {
					return;
				}

				new Cooldown(flesh, p);

				// Fire
				Block targetBlock = p.getTargetBlock((Set<Material>) null, 6);

				p.playSound(p.getLocation(), Sound.BLOCK_DISPENSER_FAIL, 1.0f, 1.0f);

				// Get local targets, try to pull them in
				for (Entity target : targetBlock.getChunk().getEntities()) {
					if (target instanceof Player) {

						if (target.getName().equals(p.getName())) {
							return;
						}

						if (targetBlock.getLocation().distance(target.getLocation()) < 4) {

							p.getWorld().playSound(target.getLocation(), Sound.BLOCK_LAVA_POP, 1.0f, 1.0f);
							Vector direction = p.getLocation().toVector().subtract(target.getLocation().toVector())
									.multiply(3).normalize();
							target.setVelocity(direction);
							return;

						}
					}
				}
			}
		}
	}
}
