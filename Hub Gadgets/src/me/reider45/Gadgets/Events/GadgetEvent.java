package me.reider45.Gadgets.Events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;

import me.reider45.Gadgets.Gadgets;

public class GadgetEvent implements Listener {

	private Gadgets main;

	public GadgetEvent(Gadgets instance) {
		main = instance;
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if (main.getGadgetHandler().isGadget(e.getItemDrop().getItemStack())) {
			e.setCancelled(true);
		}
	}

}
