package me.reider45.Gadgets.Weapons;

import java.util.Set;

import me.reider45.Gadgets.Handlers.Cooldown;
import me.reider45.Gadgets.Handlers.CooldownHandler;
import me.reider45.Gadgets.Handlers.GadgetHandler;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

public class GadgetFlesh extends Gadget implements Listener {

	public GadgetFlesh(){
		super(Material.TRIPWIRE_HOOK, "Flesh Hook", 5);
	}

	@EventHandler
	public void onShoot(PlayerInteractEvent e){
		Player p = e.getPlayer();

		if(e.getAction().name().contains("RIGHT")){
			if(p.getItemInHand() == null){ return; }

			if(GadgetHandler.isGadget(p.getItemInHand())){

				Gadget flesh = GadgetHandler.getGadget("Flesh Hook");
				if(flesh.getItem().isSimilar(p.getItemInHand())){
					e.setCancelled(true);

					// On CD still
					if(CooldownHandler.hasCooldown(p, flesh)){ return; }

					new Cooldown(flesh, p);

					// Fire
					Block targetBlock = p.getTargetBlock((Set<Material>) null, 6);
					for(Entity en : targetBlock.getChunk().getEntities()) {
						if(en instanceof Player){
							
							if(en.getName().equals(p.getName())){ return; }
							if(targetBlock.getLocation().distance(en.getLocation()) < 4){
								
								p.getWorld().playSound(en.getLocation(), Sound.LAVA_POP, 1.0f, 1.0f);
								Vector direction = p.getLocation().toVector().subtract(en.getLocation().toVector()).multiply(3).normalize();
								en.setVelocity(direction);
								return;
								
							}
						}
					} 



				}

			}
		}
	}
}
