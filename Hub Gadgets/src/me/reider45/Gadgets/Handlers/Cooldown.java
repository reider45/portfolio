package me.reider45.Gadgets.Handlers;

import me.reider45.Gadgets.Main;
import me.reider45.Gadgets.Weapons.Gadget;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Cooldown {
	
	private Player p;
	private Gadget g;
	private Long cooldown;
	private Boolean isOnCD;
	
	public Cooldown(Gadget gadget, Player p){
		this.p = p;
		this.g = gadget;
		this.cooldown = gadget.getCooldown();
		this.isOnCD = true;
		
		startCooldown();
		CooldownHandler.cooldowns.put(p, this);
	}
	
	public Gadget getGadget(){
		return this.g;
	}
	
	public Boolean isOnCD(){
		return isOnCD;
	}
	
	public void startCooldown(){
		
		new BukkitRunnable() {
			public void run(){ 
				
				this.cancel();
				isOnCD = false;
				CooldownHandler.cooldowns.remove(p);
				
			}
		}.runTaskLaterAsynchronously(Main.getInstance(), cooldown * 20);
		
	}

}
