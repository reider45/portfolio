package me.reider45.Gadgets.Weapons;

import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.inventory.ItemStack;

import me.reider45.Gadgets.Handlers.Cooldown;

public class GadgetPearl extends Gadget implements Listener {

	private Gadget pearl;

	public GadgetPearl() {
		super(Material.ENDER_PEARL, "Riding Pearl", 5);

		pearl = getGadgetHandler().getGadget("Riding Pearl");
	}

	// Shoot
	@EventHandler
	public void onShoot(PlayerInteractEvent e) {
		Player p = e.getPlayer();

		if (e.getAction().name().contains("RIGHT")) {

			ItemStack inHand = p.getInventory().getItemInMainHand();

			if (inHand != null && pearl.getItem().isSimilar(inHand)) {
				e.setCancelled(true);

				// On CD still
				if (getCooldownHandler().hasCooldown(p, pearl)) {
					return;
				}

				new Cooldown(pearl, p);

				// Fire
				Projectile proj = p.launchProjectile(EnderPearl.class);
				proj.setPassenger(p);
			}
		}
	}

	// Cancel TP
	@EventHandler
	public void pearlTP(PlayerTeleportEvent e) {
		if (e.getCause() == TeleportCause.ENDER_PEARL) {
			e.setCancelled(true);
		}
	}

}
