package me.reider45.Archery.Handlers;

import java.util.HashSet;
import java.util.Iterator;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;

import me.reider45.Archery.Archery;
import me.reider45.Archery.Arrows.ArrowType;
import me.reider45.Archery.Objects.IArrow;

public class ArrowManager {

	public HashSet<IArrow> arrows;

	public ArrowManager() {
		arrows = new HashSet<IArrow>();
		createArrows();
	}

	public void createArrows() {
		IArrow arrow_Teleport = new IArrow("swap", ArrowType.SWAP, "�7Swapping Arrow");
		IArrow arrow_Lightning = new IArrow("lightning", ArrowType.LIGHTNING, "�9Lightning Arrow");

		arrows.add(arrow_Teleport);
		arrows.add(arrow_Lightning);
	}

	public Boolean swapArrow(IArrow current, Player p) {

		Iterator<IArrow> it = arrows.iterator();

		IArrow lastChecked = current;

		while (it.hasNext()) {

			IArrow toCheck = comesAfter(lastChecked.getType());
			if (p.getInventory().containsAtLeast(toCheck.getArrow(1), 1)) {
				// Valid
				Archery.getPM().getProfile(p).setArrow(toCheck);
				break;
			}
			lastChecked = toCheck;

		}

		return false;
	}

	private IArrow comesAfter(ArrowType current) {
		switch (current) {
		case LIGHTNING:
			return getArrow("swap");
		case SWAP:
			return getArrow("lightning");
		default:
			return getArrow("lightning");
		}

	}

	// Check if custom arrow
	public IArrow isArrow(Arrow a) {

		for (IArrow b : arrows) {
			if (a.hasMetadata(b.getName()))
				return b;
		}

		return null;
	}

	// Return arrow via name
	public IArrow getArrow(String name) {
		for (IArrow b : arrows) {
			if (b.getName().equals(name))
				return b;
		}

		return null;
	}

	public void shutdown() {
		arrows.clear();
	}

}
